import pandas as pd
from sklearn.model_selection import train_test_split
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras import layers
import numpy as np

#tuning embedding matrix glove




#get data from file
filepath_dict = {'yelp':   'sentiment labelled sentences/sentiment labelled sentences/yelp_labelled.txt',
                 'amazon': 'sentiment labelled sentences/sentiment labelled sentences/amazon_cells_labelled.txt',
                 'imdb':   'sentiment labelled sentences/sentiment labelled sentences/imdb_labelled.txt'}

df_list = []

for source, filepath in filepath_dict.items():
    #set = t
    df = pd.read_csv(filepath, names=['sentence', 'label'], sep='\t')
    df['source'] = source  # Add another column filled with the source name
    df_list.append(df)


df = pd.concat(df_list)

#process word2vec
sentences = df['sentence'].values
labels = df['label'].values

sentences_train, sentences_test, y_train, y_test = train_test_split(
        sentences, labels, test_size=0.25, random_state=1000)

tokenizer = Tokenizer(num_words=20000,oov_token='[UNKNOWN]')
tokenizer.fit_on_texts(sentences_train)

X_train = tokenizer.texts_to_sequences(sentences_train)
X_test = tokenizer.texts_to_sequences(sentences_test)

vocab_size = len(tokenizer.word_index) + 1

# print(X_train)
# print(sentences_train.head)
#padding
maxlen = 100
X_train_padded = pad_sequences(X_train,padding='post', maxlen=maxlen)
X_test_padded = pad_sequences(X_test,padding='post',maxlen=maxlen)


print(X_train_padded[0, :])
print(y_train[1])
embedding_dim = 50

# model = Sequential()
# #embedding parameters
# #input_dim: the size of the vocabulary
# # output_dim: the size of the dense vector
# # input_length: the length of the sequence
# model.add(layers.Embedding(input_dim=vocab_size,
#                            output_dim=16,
#                            input_length=maxlen))
# # model.add(layers.Flatten())
# model.add(layers.GlobalAveragePooling1D())
# model.add(layers.Dense(64,activation='relu'))
# model.add(layers.Dropout(0.4))
# model.add(layers.Dense(32,activation='relu'))
# model.add(layers.Dropout(0.4))
# model.add(layers.Dense(16,activation='relu'))
# model.add(layers.Dense(1,activation='sigmoid'))
#
# model.compile(optimizer='adam',
#               loss='binary_crossentropy',
#               metrics=['accuracy'])
#
# model.summary()
#
# history = model.fit(X_train_padded, y_train,
#                     epochs=50,
#                     verbose=True,
#                     validation_data=(X_test_padded, y_test),
#                     batch_size=10)

# loss, accuracy = model.evaluate(X_train_padded, y_train, verbose=False)
# print("Training Accuracy: {:.4f}".format(accuracy))
# loss, accuracy = model.evaluate(X_test_padded, y_test, verbose=False)
# print("Testing Accuracy:  {:.4f}".format(accuracy))


def create_embedding_matrix(file_path,word_index,embedding_dim):
    vocab_size = len(word_index) + 1
    embedding_matrix = np.zeros((vocab_size, embedding_dim))
    with open(file_path) as f:
        for line in f:
            word, *vector = line.split()
            if word in word_index:
                idx = word_index[word]
                embedding_matrix[idx] = np.array(vector, dtype=np.float32)[:embedding_dim]

    return embedding_matrix
#use pretrain embedding
embedding_matrix = create_embedding_matrix(
    'glove.6B/glove.6B.50d.txt',
    tokenizer.word_index, embedding_dim)

print('embedding_matrix: ',embedding_matrix)
#axis 1 is num of rows in embedding_matrix
nonzero_elements = np.count_nonzero(np.count_nonzero(embedding_matrix, axis=1))
#xem do bao phu vocab
print(nonzero_elements / vocab_size)
print(tokenizer.word_index)

model = Sequential()
model.add(layers.Embedding(input_dim=vocab_size,output_dim=embedding_dim,
                          weights=[embedding_matrix],
                          input_length=maxlen,
                          trainable=True))

model.add(layers.pooling.GlobalAveragePooling1D())
model.add(layers.Dense(256, activation='relu'))
model.add(layers.Dense(128, activation='relu'))
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(32, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))

model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy'])

model.summary()

history = model.fit(X_train_padded, y_train,
                    epochs=200,
                    verbose=True,
                    validation_data=(X_test_padded, y_test),
                    batch_size=32)







